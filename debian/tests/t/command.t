#!/bin/sh

echo 1..3

. `dirname $0`/boilerplate.sh

screen -D -m -T linux -- ranger --cmd "touch $TESTNAME" &

i=0
while [ ! -f $TESTNAME ] ; do
	i=$((i + 1))
	[ "$i" -ge "60" ] && break
	sleep 1
done

pgrep -f $TESTNAME
check_exit_code_true Session found

test -f $TESTNAME
check_exit_code_true Touched file

pkill -f $TESTNAME
check_exit_code_true Session is gone

